from selenium import webdriver
from selenium.webdriver.common.by import By

from POM.BasePage import BasePage


class HomePage(BasePage):

    searchBar = "input[name='searchVal']"
    searchButton = "button[type='submit']"
    floatingButton = "div[class='ic-floating']"

    def __init__(self, driver):
        super().__init__(driver)

    def enterSearchItemInSearchBar(self, item):
        self.driver.find_element(By.CSS_SELECTOR, self.searchBar).send_keys(item)

    def clickOnSearchButton(self):
        self.driver.find_element(By.CSS_SELECTOR, self.searchButton).click()

    def verifyHomePage(self, title):
        self.verifyTitle(title)
        self.isVisible(self.floatingButton)
