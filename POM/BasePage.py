from selenium.webdriver.common.by import By


class BasePage:

    def __init__(self, drivers):
        self.driver = drivers

    def verifyTitle(self, title):
        assert self.driver.title == title

    def isVisible(self, locator):
        self.driver.find_element(By.CSS_SELECTOR, locator).is_displayed()
