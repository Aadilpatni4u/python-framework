import configparser

config = configparser.RawConfigParser()
# Note: Need to change path to locate config.ini
config.read("/Users/aadilpatni/PycharmProjects/BehaveDemo/resources/config.ini")


class ReadConfig:

    @staticmethod
    def getBaseUrl():
        url = config.get("common info", "baseUrl")
        return url

    @staticmethod
    def getBrowserName():
        browser = config.get("common info", "browser")
        return browser

    @staticmethod
    def getChromeDriverPath():
        path = config.get("common info", "chromeDriverPath")
        return path

    @staticmethod
    def getGeckoDriverPath():
        path = config.get("common info", "geckoDriverPath")
        return path
