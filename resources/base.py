from selenium import webdriver
from resources.readProperties import ReadConfig


def initializeDriver():
    if ReadConfig.getBrowserName() == "chrome":
        driver = webdriver.Chrome(executable_path=ReadConfig.getChromeDriverPath())
    elif ReadConfig.getBrowserName() == "firefox":
        driver = webdriver.Firefox(executable_path=ReadConfig.getGeckoDriverPath())
    elif ReadConfig.getBrowserName() == "safari":
        driver = webdriver.Safari()
    driver.maximize_window()
    driver.implicitly_wait(10)
    return driver
