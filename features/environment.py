from resources.base import initializeDriver


def before_scenario(context, scenario):
    context.driver = initializeDriver()


def after_scenario(context, scenario):
    if scenario.status == "failed":
        context.driver.save_screenshot("Screenshots/{}.png".format(context.scenario.name))

    context.driver.close()


