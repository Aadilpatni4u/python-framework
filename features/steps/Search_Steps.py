from behave import *

from POM.HomePage import HomePage
from resources.base import initializeDriver
from resources.readProperties import ReadConfig


@given(u'User is on Home page')
def user_is_on_home_page(context):
    context.driver.get(ReadConfig.getBaseUrl())
    context.homePage = HomePage(context.driver)
    context.homePage.verifyHomePage("Online Shopping for Women, Men, Kids – Clothing, Footwear | AJIO")


@when(u'User enters "{item}" as input')
def user_enter_item_as_input(context, item):
    context.homePage.enterSearchItemInSearchBar(item)


@when(u'User click on search button')
def user_click_on_search_button(context):
    context.homePage.clickOnSearchButton()


@then(u'User should be able to see results for "{item}"')
def user_should_see_results(context, item):
    actualTitle = context.driver.title
    assert actualTitle == item
