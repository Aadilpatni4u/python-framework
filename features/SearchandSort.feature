Feature: Test Search and Sorting Feature

  Background: common steps
    Given User is on Home page


 Scenario Outline: Search With Valid Input
   When User enters "<items>" as input
   And User click on search button
   Then User should be able to see results for "<items>"


   Examples:
     | items  |
     | Jeans  |
     | Shirts |

Scenario Outline: Sort The Results
    When User enters "<items>" as input
    And User click on search button
    When User Click on Sort By and select Price(lowest first)
    Then Result Get Sorted in Ascending Order
  Examples:
    | items |
    | Jeans |